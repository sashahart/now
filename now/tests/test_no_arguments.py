
import subprocess


def test_no_arguments():
    process = subprocess.Popen(["now"], stderr=subprocess.PIPE)
    output, errput = process.communicate(input=b"")
    assert process.returncode == 1
    assert errput.startswith(b"Error: ")
