import subprocess


def test_help_option():
    process = subprocess.Popen(["now", "--help"], stdout=subprocess.PIPE)
    output, errput = process.communicate(input=b"")
    assert process.returncode == 0
    assert output.startswith(b"usage: now")
