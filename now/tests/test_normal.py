import os
import subprocess
import tempfile


def run(path, args):
    process = subprocess.Popen(
        ["now"] + args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        env={'PATH': os.environ['PATH'],
             'NOWFILE': path},
    )
    output, errput = process.communicate(input=b"")
    assert process.returncode == 0
    assert output == b""
    assert errput == b""


def test_normal():
    # Can't fake filesystem out of process, so NOWFILE is implemented to
    # let us specify a temporary path which we can write to, read from,
    # and then remove. Meh.
    tmp_handle, tmp_path = tempfile.mkstemp()
    try:
        run(tmp_path, ["doing", "things", "1"])
        run(tmp_path, ["doing", "things", "2"])
        with open(tmp_path, 'rb') as inp:
            written = inp.readlines()
    finally:
        os.remove(tmp_path)
    assert written[0].endswith(b" doing things 1\n")
    assert written[1].endswith(b" doing things 2\n")
